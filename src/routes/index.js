import { Router } from 'express'
import ApiError from '../errors/ApiError';

const router = new Router()

router.get('/', (req, res) => res.send('Hello World'))
router.get('/error', (req, res, next) => {
    const query = req.query
    
    if (!Object.keys(query).length) {
        return next(ApiError.badRequest('Error: lost params'))
    }
    
    res.json(query)
})

export default router
