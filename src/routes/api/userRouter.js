import { Router } from 'express'
import userController from '../../controllers/userController'

const router = new Router()

router.get('/', userController.getCurrentUser)
router.get('/auth', userController.check)
router.post('/login', userController.login)
router.post('/registration', userController.registration)

export default router
