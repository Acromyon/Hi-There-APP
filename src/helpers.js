import fs from 'fs'

export const checkAndMakeDir = (dir) => {
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir, { recursive: true })
        console.log(`create directory: "${dir}"`)
    }
}

export const findFirstAndLastDayOfMonth = (byDate = new Date()) => {
    const firstDay = new Date(byDate.getFullYear(), byDate.getMonth(), 1);
    const lastDay = new Date(byDate.getFullYear(), byDate.getMonth() + 1, 0);
    
    return [
        firstDay,
        lastDay,
    ]
}
