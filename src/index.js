import Express from 'express'
import fileUpload from 'express-fileupload'
import path from 'path'
import { fileURLToPath } from 'url'
import cors from 'cors'
import dotenv from 'dotenv'
dotenv.config()

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const staticDir = path.resolve(__dirname, '..', 'static')
const PORT = process.env.PORT || 7007

import sequelize from './db.js'
import errorHandler from './middleware/ErrorHandlingMiddleware'
import router from './routes'
import apiRouter from './routes/api'
import { checkAndMakeDir } from './helpers'

checkAndMakeDir(staticDir)

const app = Express()

app.use(cors())
app.use(Express.json())
app.use('/static', Express.static(staticDir))
app.use(fileUpload({}))
app.use('/', router)
app.use('/api/v1', apiRouter)
app.use(errorHandler) // should be the last middleware(!)

const startApp = async () => {
    try {
        await sequelize.authenticate()
        await sequelize.sync()
        app.listen(PORT, () => console.log(`Server is running on http://localhost:${PORT}`))
    } catch(err) {
        console.log(err);
    }
}

startApp()
