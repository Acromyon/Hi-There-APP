import path from 'path'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { v4 as uuidv4 } from 'uuid'
import { fileURLToPath } from 'url'
import dotenv from 'dotenv'
dotenv.config()

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

import ApiError from '../errors/ApiError'
import { User } from '../models'

const generateJwt = (id, email) => (
    jwt.sign({
            id,
            email
        }, process.env.SECRET_KEY, { expiresIn: '24h' })
)

class UserController {
    async getCurrentUser(req, res, next) {
        try {
            const { id } = req.query // TODO get current user from session
            
            const user = await User.findOne({ where: { id }})
            
            return res.json(user)
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async check(req, res, next) {
        try {
        
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async login(req, res, next) {
        try {
            const { email, password } = req.body
            
            const user = await User.findOne({ where: { email } })
            
            if (!user) {
                return next(ApiError.badRequest('Пользователь не найден'))
            }
            
            const comparePassword = bcrypt.compareSync(password, user.password)
            
            if(!comparePassword) {
                return next(ApiError.badRequest('Указан неверный пароль'))
            }
            
            const jwt = generateJwt(user.id, email)
            
            return res.json({ user, jwt })
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async registration(req, res, next) {
        try {
            const { name, email, password } = req.body
            const isEmailAlreadyExists = await User.findOne({ where: { email } })
            
            if (isEmailAlreadyExists) {
                return next(ApiError.badRequest('Пользователь с таким email уже существует'))
            }
            
            const hashPassword = await bcrypt.hash(password, 5)
            let fileName
            
            if (req.files && req.files.img) {
                fileName = uuidv4() + '.jgp'
                const img = req.files.img
                await img.mv(path.resolve(__dirname, '..', '..', 'static', fileName))
            }
            
            const user = await User.create({
                name,
                email,
                password: hashPassword,
                img: fileName,
            })
            
            const jwt = generateJwt(user.id, email)
            
            return res.json({ user, jwt })
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
}

export default new UserController
