import { Op } from 'sequelize'
import ApiError from '../errors/ApiError'
import { findFirstAndLastDayOfMonth } from '../helpers'
import { Event } from '../models'

class EventController {
    async getAll(req, res, next) {
        try {
            const {
                dateFrom,
                dateTo,
                amountFrom,
                amountTo,
                userId,
                eventTypeId
            } = req.query
            
            const events = await Event.findAll({
                where: {
                    userId, // TODO get current user from session
                    ...(eventTypeId && { eventTypeId }),
                    
                    ...((dateFrom || dateTo) ? {
                        date: {
                            ...(dateFrom && { [Op.gte]: dateFrom }),
                            ...(dateTo && { [Op.lte]: dateTo }),
                        },
                    } : {
                        date: {
                            [Op.between]: findFirstAndLastDayOfMonth(),
                        },
                    }),

                    ...((amountFrom || amountTo) && {
                        amount: {
                            ...(amountFrom && { [Op.gte]: amountFrom }),
                            ...(amountTo && { [Op.lte]: amountTo }),
                        }
                    })
                }
            })
            
            return res.json(events)
        
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async getOne(req, res, next) {
        try {
            const { id } = req.params
            
            const event = await Event.findOne({ where: { id } })
            
            // TODO check current user access rights
            
            return res.json(event)
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
    
    async create(req, res, next) {
        try {
            const { date, amount, userId, eventTypeId } = req.body
            
            const event = await Event.create({
                date,
                amount,
                userId,
                eventTypeId,
            })
            
            return res.json(event)
        } catch (err) {
            next(ApiError.badRequest(err.message))
        }
    }
}

export default new EventController
